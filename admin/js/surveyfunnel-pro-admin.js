/**
 * Admin JavaScript.
 *
 * @package    Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/admin
 **/

(function( $ ) {
	'use strict';

	$( document ).ready(function() {
		$('[filter-search]').on('change', function() {
			let searchVal       = this.value;
			let filterItems = $('[data-filter-item]');
			let indexSearchVal  = 'all-types';
			if ( searchVal !== 'all-types' ) {
				filterItems.addClass('d-none');
				switch( searchVal ) {
					case 'basic-survey':
						indexSearchVal = 'basic';
						break;
					case 'scoring-logic':
						indexSearchVal = 'scoring';
						break;
					case 'outcome-logic':
						indexSearchVal = 'outcome';
						break;
				}
				$('[data-filter-item][data-filter-type="' + indexSearchVal + '"]').removeClass('d-none');
			} else {
				filterItems.removeClass('d-none');
			}
		})
	});

})( jQuery );
