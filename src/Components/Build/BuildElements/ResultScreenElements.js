const { addFilter } = wp.hooks;
import React from 'react';
import '../../../scss/resultScreenElements.scss';
import '../../../scss/common.scss';


addFilter( 'resultScreenState', 'ResultScreenElements', resultScreenState );

function resultScreenState( data, type ) {
	if( type === 'scoring' ) {
		return {
			startRange: 0,
			endRange: 0,
			showResult: false,
			displayMessage: 'Your Score is',
		}
	}
	return {};
}

addFilter( 'resultScreenSetComponentMount', 'ResultScreenElements', resultScreenSetComponentMount );

function resultScreenSetComponentMount( data, type, currentELement ) {
	if ( type === 'scoring' ) {
		return {
			startRange: currentELement?.startRange || 0,
			endRange: currentELement?.endRange || 0,
			showResult: currentELement.hasOwnProperty( 'showResult' ) ? currentELement.showResult : false,
			displayMessage: currentELement?.displayMessage || 'Your Score is',
		}
	}
	return {};
}

addFilter( 'resultScreenLeftElementsRender', 'ResultScreenElements', resultScreenLeftElementsRender );

function resultScreenLeftElementsRender(data, type, state, handleResultChange) {

	if ( type === 'scoring' ) {
		return <div className="modalComponentResult">
			<h3>Result Settings</h3>
			<p>The score that will trigger this result ranges from:</p>
			
			<div className="rangeInput">
				<input type="number" name="startRange" className="startRange" min="0" max="100" value={state.startRange} onChange={(e) => {handleResultChange(e)}} />
				to
				<input type="number" name="endRange" className="endRange" min="0" max="100" value={state.endRange} onChange={(e) => {handleResultChange(e)}} />
			</div>

			<div className="showScoreOnResultPage">
				<p>Display score on the result page</p>
				<input type="checkbox" id="showResult" name="showResult" checked={state.showResult} onChange={(e) => {handleResultChange(e, true)}} />
				<label htmlFor="showResult"></label>
			</div>

			<div className="scoreDisplayMessage">
				<span>Message for displayed score:</span>
				<input type="text" name="displayMessage" value={state.displayMessage} onChange={(e) => {handleResultChange(e)}} />
			</div>
		</div>
	}

	return '';
}

addFilter( 'resultScreenRightElementsRender', 'ResultScreenElements', resultScreenRightElementsRender );

function resultScreenRightElementsRender(data, type, state) {
	if ( type === 'scoring' ) {
		if ( state.showResult ) {
			return <div className="showResult">
				{state.displayMessage} <strong>50</strong>
			</div>
		}
		return '';
	}
	return '';
}

addFilter( 'resultScreenValidation', 'ResultScreenElements', resultScreenValidation );

function resultScreenValidation( errorArray, state, List ) {
	
	// check for if startrange is greater than end range
	if ( Number( state.startRange ) > Number( state.endRange ) ) {
		errorArray.push( <p className="errorResultScreen" key={errorArray.length}>Start Range cannot be greater than end range</p> );
		return errorArray;
	}
	// check if it overlaps with any other results
	const { RESULT_ELEMENTS } = List;
	for(let i = 0 ; i < RESULT_ELEMENTS.length ; i++) {
		if ( RESULT_ELEMENTS[i].id === state?.id ) {
			continue;
		}
		if ( Number( RESULT_ELEMENTS[i].startRange ) <= Number( state.startRange ) && Number( RESULT_ELEMENTS[i].endRange ) >= Number( state.startRange ) ) {
			errorArray.push( <p className="errorResultScreen" key={errorArray.length}>The min score range is overlapping with the score set on another result page</p> );
			return errorArray;
		} else if( Number( RESULT_ELEMENTS[i].startRange ) <= Number( state.endRange ) && Number( RESULT_ELEMENTS[i].endRange ) >= Number( state.endRange ) ) {
			errorArray.push( <p className="errorResultScreen" key={errorArray.length}>The max score range is overlapping with the score set on another result page</p> );
			return errorArray;
		}
	}
	return errorArray;
}

let exportFunctionsForTesting = { resultScreenState, resultScreenSetComponentMount, resultScreenLeftElementsRender, resultScreenRightElementsRender, resultScreenValidation };
export default exportFunctionsForTesting;