import { Component } from 'react';
import React from 'react';
export default class TextElement extends Component {
	state = {
		title: "",
		description: "",
		error: "",
		proVersionQuestionType: true,
	}

	handleChange = (event) => {
		this.setState({
			[event.target.name]: event.target.value,
		});		
	};

	handleSave = () => {
		let err = '';
		if ( this.state.title !== '' ) {
			err = '';
		}
		else {
			err = 'Please add title before saving';
		}
		this.setState({
			error: err
		}, () => {
			if ( err === '' ) {
				this.props.saveToList();
			}
		})
	}

	componentDidMount() {
		const { currentElement } = this.props;
		if ( 'currentlySaved' in currentElement ) {
			let state = {
				button: currentElement.button,
				title: currentElement.title,
				description: currentElement.description,
			}
			this.setState(state);
		}
	}

	checkForEmpty( name ) {
		if ( this.state[name] === '' ) {
			return false;
		}
		return true;
	}

	render() {
		const { designCon, currentElement, CloseModal, ModalContentRight } = this.props;
		return (
			<>
				<div className="modalOverlay">
					<div className="modalContent-navbar">
					<h3>Start Screen  &nbsp; &#62; &nbsp;  Cover Page  &nbsp; &#62; &nbsp;  {this.state.title}</h3>
						<CloseModal/>
					</div>
					<div className="modalContent">
						<div className="modalContent-left">
							<div className="modalContent-left-fields">
								<div className="modalComponentTitle">
									<h3>Title</h3>
									<input
										type="text"
										placeholder="Set Title"
										name="title"
										value={this.state.title}
										onChange={this.handleChange}
									/>
								</div>
								<div className="modalComponentDescription">
									<h3>Description</h3>
									<textarea
										type="text"
										placeholder="Set Description"
										name="description"
										value={this.state.description}
										onChange={this.handleChange}
										style={{height:"150px"}}
									/>
								</div>                       
								<div className="modalComponentError">
									{this.state.error}
								</div>                                
							</div>
							<div className="modalComponentSaveButton">
								<button name="save" onClick={this.handleSave}>Save</button>
							</div>
						</div>
						<div className="modalContent-right">
							<div className="modalContentMode">
								<h4>Content Preview</h4>
							</div>
							<div className="modalContentPreview">
							{this.state.title === '' && this.state.description === '' ? 
							(<div className="no-preview-available">
								<img></img>
								<div>
									No preview available
								</div>
							</div>)
								: (<ModalContentRight designCon={designCon} currentElement={currentElement.componentName}>
									{ this.checkForEmpty('title') && <h3 className="surveyTitle">{this.state.title}</h3> }
									{ this.checkForEmpty('description') && <p className="surveyDescription">{this.state.description}</p> }
							</ModalContentRight>)}
							</div>
						</div>
					</div>
				</div>
			</>
		);
	}
}