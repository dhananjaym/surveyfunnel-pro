const { addFilter } = wp.hooks;
import React from 'react';

addFilter( 'scoringLogicCardFilter', 'ContentElements', scoringLogicCardFilter );

function scoringLogicCardFilter( data, item, type ) {
	if( type === 'scoring' ) {
		return <label style={{padding: '1px 6px'}}>Score: {item.startRange} to {item.endRange}</label>
	}
	return '';
}

let exportFunctionsForTesting = { scoringLogicCardFilter };
export default exportFunctionsForTesting;