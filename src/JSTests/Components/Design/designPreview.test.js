import testFunctions from '../../../Components/Design/DesignPreview.js';
import React from 'react';

function ShowErrors() {
	return <div></div>
}

test( 'Start Screen Privacy Policy checkbox render in modalbox', () => {
	let data = '';
	let proSettings = {
		privacyPolicy: {
			text: 'something',
			link: {
				value: 'http://www.google.com'
			}
		}
	};
	let state = {
		privacyPolicy: true,
	}
	let jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);

	expect(jsx.props.children.type === 'div').toBeTruthy();
	state.privacyPolicy = false;
	jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();

	proSettings.privacyPolicy.text = '';
	jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();
} )


test( 'render content elements design preview', () => {

	let jsx = testFunctions.renderContentElementsDesignPreview(  '', {
		componentName: 'hello',
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1 );

	expect(jsx).toBe('');

	jsx = testFunctions.renderContentElementsDesignPreview(  '', {
		componentName: 'ImageQuestion',
		answers: [
			{
				imageUrl: 'http://www.google.com/images/car.jpg'
			},
		]
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1 );

	expect(jsx.props.className).toBe('surveyfunnel-lite-tab-ImageQuestion');

	jsx = testFunctions.renderContentElementsDesignPreview(  '', {
		componentName: 'TextElement',
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1 );

	expect(jsx.props.className).toBe('surveyfunnel-lite-tab-TextElement');

} );

test('calling render content elements as per element type', () => {
	let data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'TextElement',
	}, 10);

	expect(data).toBe('HelloWorld');
	data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'ImageQuestion',
	}, 10);

	expect(data).toBe('HelloWorld');

	data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'SomethingElse',
	}, 10);

	expect(data).toBe('');
})
