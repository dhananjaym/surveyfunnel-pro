import testFunction from '../../../../Components/Build/BuildElements/ContentElements.js';

test( 'to check if state of choices component will be modified or not interms of survey type', () => {
	let data = testFunction.choicesState({}, 'scoring');
	expect( data ).toEqual({score: 0});
	expect(testFunction.choicesState({}, 'basic')).toEqual({});
} )

test( 'Single/Multichoice score box getting rendered or not', () => {
	let jsx = testFunction.answersLeftBoxes( '', 'scoring', {score: 20}, () => {}, 1 );
	expect(jsx.props.value).toBe(20);
} );