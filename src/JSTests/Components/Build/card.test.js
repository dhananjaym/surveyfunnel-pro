import testFunction from '../../../Components/Build/Card.js';

test( 'to check if card appears with its scores in scoring logic', () => {
	let item = {
		startRange: '80',
		endRange: '100',
	}
	let jsx = testFunction.scoringLogicCardFilter('', item, 'scoring');
	expect(jsx.props.children[1]).toBe(item.startRange);
	expect(jsx.props.children[3]).toBe(item.endRange);

	jsx = testFunction.scoringLogicCardFilter('', item, 'basic');
	expect(jsx).toBe('');
} )