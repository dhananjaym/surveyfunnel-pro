import '@testing-library/jest-dom';
import { configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

global.wp = {
	hooks: {
		addFilter: function(num1, num2, callback) {return true;},
		addAction: function(num1, num2, callback) {return true;}
	}
}

configure({adapter: new Adapter()});